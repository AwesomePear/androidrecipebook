package com.example.bestkursachever;

import android.content.ContentValues;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class Manager {
    private List<Category> mCategories;
    private List<Recipe> mRecipes;
    private static Manager Manager;


    private Manager() {
        mCategories = new ArrayList<>();
        mRecipes = new ArrayList<>();
    }

    public static Manager get(){return Manager == null? Manager=new Manager() : Manager; }
    public List<Category> getCategories() { return mCategories; }
    public List<Recipe> getRecipes() { return mRecipes; }

    public void loadRecipe(Recipe r){mRecipes.add(r);}
    public void loadCategory(Category category){mCategories.add(category);}
    public void recreate(){
        Manager = new Manager();
    }


}