package com.example.bestkursachever;

import java.util.List;

public class Recipe {
    private int Id;
    private String name;
    //private int imageResourceId;
    private byte[] img;
    private String time;
    private int CategoryId;

    private String Ingredients;
    private String Description;


    public Recipe(int id_r, String recipeName, byte[] img, String t, int cat_id){
        this.Id = id_r;
        this.img = img;
        this.name = recipeName;
        this.time = t;
        this.CategoryId = cat_id;
    }

    public Recipe(int id_r, String recipeName, byte[] img, String t, int cat_id, String Ingrs,String Desc){
        this.Id = id_r;
        this.img = img;
        this.name = recipeName;
        this.time = t;
        this.CategoryId = cat_id;
        Ingredients = Ingrs;
        Description = Desc;
    }


    public int getId() {
        return Id;
    }
    public int getCategoryId() {
        return CategoryId;
    }
    public byte[] getImageBytes() {
        return img;
    }
    public String getName() {
        return name;
    }
    public String getTime() {
        return time;
    }

    public String getDescription() {
        return Description;
    }
    public String  getIngredients() {
        return Ingredients;
    }

    @Override
    public String toString() {
        return name+": ";
    }
}
