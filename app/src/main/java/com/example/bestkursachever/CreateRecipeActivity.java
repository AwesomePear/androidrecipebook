package com.example.bestkursachever;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class CreateRecipeActivity extends AppCompatActivity {
    private static final int req_code = 1;
    private final int REQUEST_CODE_PERMISSION_GALLERY = 10;
    private ImageView imageView;
    private Bitmap Current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_recipe);
        ((TextView)findViewById(R.id.toolbar_title)).setText(R.string.dish_descr_activity_create);
        imageView = findViewById(R.id.photo);
    }

    public void onImageClick(View view){
        int permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permissionStatus== PackageManager.PERMISSION_DENIED)
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION_GALLERY);
        if (permissionStatus== PackageManager.PERMISSION_DENIED) return;
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        //Тип получаемых объектов - image:
        photoPickerIntent.setType("image/*");
        //Запускаем переход с ожиданием обратного результата в виде информации об изображении:
        startActivityForResult(photoPickerIntent, req_code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case req_code:
                if(resultCode == RESULT_OK){
                    try {
                        //Получаем URI изображения, преобразуем его в Bitmap
                        //объект и отображаем в элементе ImageView нашего интерфейса:
                        final Uri imageUri = imageReturnedIntent.getData();
                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        Current = selectedImage;
                        imageView.setImageBitmap(selectedImage);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    public void onAddButtonClick(View view){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] byteArray = new byte[]{};
        if (Current!=null) {
            Current.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
        }
        new AsyncInsert().execute(
                ((EditText) findViewById(R.id.editview_name)).getText().toString(),
                ((EditText) findViewById(R.id.editview_desc)).getText().toString(),
                ((EditText) findViewById(R.id.editview_ingr)).getText().toString(),
                ((EditText) findViewById(R.id.editview_time)).getText().toString(),
                ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString(),
                byteArray
        );
        finish();

    }
}
