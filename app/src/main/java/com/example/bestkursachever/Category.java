package com.example.bestkursachever;

import java.util.ArrayList;

public class Category {
    private int Id;
    private String name;
    private int imageResourceId;

    public Category(int id_cat, String categoryName, int id){
        this.Id = id_cat;
        this.imageResourceId = id;
        this.name = categoryName;
    }
    public int getId() {
        return Id;
    }
    public int getImageId() {
        return imageResourceId;
    }
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name+": ";
    }
}
