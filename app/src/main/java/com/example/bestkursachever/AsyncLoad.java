package com.example.bestkursachever;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.ResultSet;
import java.sql.SQLException;

    public class AsyncLoad extends AsyncTask<Object,Void,Boolean> {

        @Override
        protected Boolean doInBackground(Object... objects) {
            Context context = (Context) objects[0];
            Manager.get().recreate();
            ResultSet res = DB.get().SelectQuery("Select * from category;");
            try{
                while (res.next()) {
                    String cat_id = res.getString("cat_id");
                    String cat_name = res.getString("cat_name");
                    String cat_img = res.getString("cat_img");
                    int img_id = context.getResources().getIdentifier(cat_img, "drawable", context.getPackageName());

                    Manager.get().loadCategory(new Category(Integer.parseInt(cat_id),cat_name,img_id));
                }
                res = DB.get().SelectQuery("Select * from recipe;");
                while (res.next()) {
                    String r_id = res.getString("r_id");
                    String r_name = res.getString("r_name");
                    //String r_img = res.getString("r_img");
                    //int img_id = context.getResources().getIdentifier(r_img, "drawable", context.getPackageName());
                    byte[] r_img = res.getBytes("r_img");
                    String r_time = res.getString("r_time");
                    String r_cat_id = res.getString("r_cat_id");
                    String r_ingrs = res.getString("r_ingrs");
                    String r_descr = res.getString("r_descr");
                    Manager.get().loadRecipe(new Recipe(Integer.parseInt(r_id), r_name, r_img, r_time, Integer.parseInt( r_cat_id), r_ingrs, r_descr));
                }

            } catch (SQLException e) {
                Log.d("DB: ",e.getMessage());
            }
            return true;
        }

    }

