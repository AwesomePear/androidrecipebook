package com.example.bestkursachever;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DishDescriptionActivity extends AppCompatActivity {
    public static final String EXTRA_RECIPE_ID = "recipeId";
    private Context context;
    private Recipe Recipe;
    private Button IngrBtn;
    private Button DescBtn;
    private TextView Name;
    private ImageView Image;
    private TextView Time;
    private TextView Ingr_Descr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_description);
        Image = (ImageView)findViewById(R.id.dish_image);
        IngrBtn = (Button)findViewById(R.id.ingredientsButton);
        DescBtn = (Button)findViewById(R.id.descriptionButton);
        Name = (TextView)findViewById(R.id.dish_name);
        Ingr_Descr = (TextView)findViewById(R.id.ingr_descr);
        context = this;
        ((TextView)findViewById(R.id.toolbar_title)).setText(R.string.dish_descr_activity_title);
        Ingr_Descr.setMovementMethod(new ScrollingMovementMethod());
        int recipeId = (Integer)getIntent().getExtras().get(EXTRA_RECIPE_ID); //взять из интента рецепт
        for (Recipe r : Manager.get().getRecipes()) if (r.getId()==recipeId) {
            Recipe = r;

            //Drawable drawable = ContextCompat.getDrawable(context, r.getImageId());
            //Image.setImageDrawable(drawable);
            byte[] byteArray = Recipe.getImageBytes();
            if (byteArray!=null) {
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                Image.setImageBitmap(bmp);
            }
            Name.setText(r.getName());
            IngrBtn.setBackgroundResource(R.drawable.gradient2_pressed);
            if (r.getIngredients()!=null) Ingr_Descr.setText(r.getIngredients().toString()); //?????
        }

    }

    public void onShowIngrsButtonClick(View view){
        Ingr_Descr.clearComposingText();
        IngrBtn.setBackgroundResource(R.drawable.gradient2_pressed);
        DescBtn.setBackgroundResource(R.drawable.gradient2);
        if (Recipe.getIngredients()!=null) Ingr_Descr.setText(Recipe.getIngredients().toString());

    }
    public void onShowDescrButtonClick(View view){
        Ingr_Descr.clearComposingText();
        IngrBtn.setBackgroundResource(R.drawable.gradient2);
        DescBtn.setBackgroundResource(R.drawable.gradient2_pressed);
        Ingr_Descr.setText(Recipe.getDescription());
    }
}
