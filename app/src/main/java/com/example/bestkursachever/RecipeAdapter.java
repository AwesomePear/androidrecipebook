package com.example.bestkursachever;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.io.ByteArrayInputStream;
import java.util.List;

class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> {
    private android.content.Context Context;
    private List<Recipe> Recipes;

    private Listener listener;
    interface Listener {
        void onClick(int position);
    }

    public void setListener(Listener listener){this.listener = listener;}/*активности и вью регистрируют себя в кач-ве слушателя*/

    public RecipeAdapter(android.content.Context context, List<Recipe>  recipes){
        Context = context;
        Recipes = recipes;
    }

    @Override
    public int getItemCount(){
        return Recipes.size();
    }

    /*создает 1 вьюхолдер*/
    @Override
    public RecipeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).
                inflate(R.layout.recipe_card, parent, false);
        return new ViewHolder(/*Context,*/cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        CardView cardView = holder.cardView;
        final Recipe recipe = Recipes.get(position);
        holder.bindCategory(recipe);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onClick(position);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // private Context Context;
        private CardView cardView;
        private Recipe Recipe;
        private TextView NameView;
        private TextView TimeView;
        private ImageView imageView;

        public ViewHolder(/*Context context,*/CardView v) {
            super(v);
            // Context = context;
            cardView = v;
            imageView = (ImageView)cardView.findViewById(R.id.dish_image);
            NameView = (TextView)cardView.findViewById(R.id.recipe_name);
            TimeView = (TextView)cardView.findViewById(R.id.recipe_time);
        }

        public void bindCategory(Recipe recipe) {//Заполнение
            Recipe = recipe;

            //Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), Recipe.getImageId());
            byte[] byteArray = Recipe.getImageBytes();
            if (byteArray!=null) {
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                imageView.setImageBitmap(bmp);
            }
            NameView.setText(Recipe.getName());
            TimeView.setText(Recipe.getTime());
        }

    }
}



