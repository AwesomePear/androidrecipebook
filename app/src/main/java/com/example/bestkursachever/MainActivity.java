package com.example.bestkursachever;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void onAddButtonClick(View view){
        Intent intent = new Intent(this, CreateRecipeActivity.class);
        startActivity(intent);
    }

    public void onFindButtonClick(View view){
        Intent intent = new Intent(this, RecipesCategoriesActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onResume(){
        super.onResume();
        new AsyncLoad().execute(this);
    }
}
