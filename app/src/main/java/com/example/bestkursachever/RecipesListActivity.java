package com.example.bestkursachever;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecipesListActivity extends AppCompatActivity {
    public static final String EXTRA_CATEGORY_ID = "categoryId"; /*id категории для выбора таблицы с рецептами*/
    private RecyclerView mRecipeRecyclerView;
    private RecipeAdapter mAdapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_list);
        context = this;
        ((TextView)findViewById(R.id.toolbar_title)).setText(R.string.categories_activity_title);
        mRecipeRecyclerView = (RecyclerView)findViewById(R.id.recipes_recycler); //взяли рециклер рецептов
        mRecipeRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        int categoryId = (Integer)getIntent().getExtras().get(EXTRA_CATEGORY_ID);/*получить из интента категорию*/
        final List<Recipe> Recipes = new ArrayList<Recipe>();
        /*заполнить рециклер в соответствии с выбранной категорией - categoryId*/
        for (Recipe r : Manager.get().getRecipes()) if (r.getCategoryId()==categoryId) Recipes.add(r);
        mAdapter = new RecipeAdapter(this, Recipes);
        mRecipeRecyclerView.setAdapter(mAdapter);
        mAdapter.setListener(new RecipeAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(context, DishDescriptionActivity.class);
                intent.putExtra(DishDescriptionActivity.EXTRA_RECIPE_ID, Recipes.get(position).getId()); // передаем айди рецепта по позиции
                context.startActivity(intent);
            }
        });
    }
}
