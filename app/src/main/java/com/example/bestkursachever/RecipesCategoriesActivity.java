package com.example.bestkursachever;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class RecipesCategoriesActivity extends AppCompatActivity {
    private RecyclerView mCategoryRecyclerView;
    private CategoryAdapter mAdapter;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes_categories);
        context = this;
        ((TextView)findViewById(R.id.toolbar_title)).setText(R.string.categories_activity_title);
        mCategoryRecyclerView = (RecyclerView)findViewById(R.id.categories_recycler);
        mCategoryRecyclerView.setLayoutManager(new GridLayoutManager(this,2));

        final List<Category> Categories = Manager.get().getCategories();
        mAdapter = new CategoryAdapter(this, Categories , Manager.get());
        mCategoryRecyclerView.setAdapter(mAdapter);

        mAdapter.setListener(new CategoryAdapter.Listener() {
            public void onClick(int position) {
                Intent intent = new Intent(context, RecipesListActivity.class);
                intent.putExtra(RecipesListActivity.EXTRA_CATEGORY_ID, Categories.get(position).getId()); // передаем айди категории по позиции
                context.startActivity(intent);
            }
        });
    }
}


