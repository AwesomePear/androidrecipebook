package com.example.bestkursachever;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


public class DB {
    private static DB DB = new DB();
    public static DB get(){return DB;}
    private DB(){tryConnect();}

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            dbConnection = DriverManager.getConnection("jdbc:postgresql://10.0.2.2:5432/Kkkk", "pukker", "22");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

    private void tryConnect() {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
        } catch (Exception e) {
            Log.d("DB: ","PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }
        Log.d("DB: ","PostgreSQL JDBC Driver successfully connected");
        Connection connection = getDBConnection();
        if (connection != null) {
            Log.d("DB: ","You successfully connected to database now");
        } else {
            Log.d("DB: ","Failed to make connection to database");
        }
    }

    public PreparedStatement getInsertStatement(){
        try{
            return getDBConnection().prepareStatement("INSERT INTO recipe(r_name, r_time, r_cat_id, r_ingrs, r_descr, r_img) VALUES(?,?,?,?,?,?);");
        }catch (SQLException e) {
            Log.d("DB: ",e.getMessage());
        }
        return null;
    }

    public ResultSet SelectQuery(String s){
        ResultSet rs = null;
        try {
            Log.d("DB: ",s);
            Statement statement = getDBConnection().createStatement();
            rs = statement.executeQuery(s); // выбираем данные с БД
        } catch (SQLException e) {
            Log.d("DB: ",e.getMessage());
        }
        return rs;
    }
}

