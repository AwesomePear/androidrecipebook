package com.example.bestkursachever;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
   // private String[] categoryNames;
    //private int[] imageIds;
    private android.content.Context Context;
    private List<Category> Categories;
    private Manager manager;

    private Listener listener;
    interface Listener {
        void onClick(int position);
    }

    public void setListener(Listener listener){this.listener = listener;}/*активности и вью регистрируют себя в кач-ве слушателя*/

    public CategoryAdapter(Context context, List<Category>  categories, Manager cmanager){
        Context = context;
        Categories = categories;
        manager = cmanager;
    }

    @Override
    public int getItemCount(){
        return Categories.size();
    }

    /*создает 1 вьюхолдер*/
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).
                inflate(R.layout.category_card, parent, false);
        return new ViewHolder(/*Context,*/cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        CardView cardView = holder.cardView;
        final Category category = Categories.get(position);
        holder.bindCategory(category);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) listener.onClick(position);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
       // private Context Context;
        private CardView cardView;
        private Category Category;
        private TextView NameView;
        private ImageView imageView;

        public ViewHolder(/*Context context,*/CardView v) {
            super(v);
           // Context = context;
            cardView = v;
            imageView = (ImageView)cardView.findViewById(R.id.info_image);
            NameView = (TextView)cardView.findViewById(R.id.info_text);

        }

        public void bindCategory(Category category) {//Заполнение
            Category = category;

            Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), Category.getImageId());
            imageView.setImageDrawable(drawable);
            NameView.setText(Category.getName());

        }

    }
}



