package com.example.bestkursachever;

import android.os.AsyncTask;
import android.util.Log;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AsyncInsert extends AsyncTask<Object,Void,Void> {

    @Override
    protected Void doInBackground(Object... objects) {
        int cat_id = -1;
        int time = 0;
        try { time = Integer.parseInt(objects[3].toString());} catch(Exception e) {time=0;}
        for (Category c: Manager.get().getCategories())
            if (c.getName().equals(objects[4].toString())) cat_id = c.getId();
        try{
            PreparedStatement s = DB.get().getInsertStatement();
            s.setBytes(6,(byte[]) objects[5]);
            s.setInt(2,time);
            s.setInt(3,cat_id);
            s.setString(1,objects[0].toString());
            s.setString(4,objects[2].toString());
            s.setString(5,objects[1].toString());
            s.execute();
        }catch (SQLException e) {
            Log.d("DB: ",e.getMessage());
        }
        return null;
    }

    /*
    INSERT INTO recipe(
            r_id, r_name, r_time, r_cat_id, r_ingrs, r_descr, r_img)
    VALUES (?, ?, ?, ?, ?, ?, ?);

                ((EditText) findViewById(R.id.editview_name)).getText().toString(),
                ((EditText) findViewById(R.id.editview_desc)).getText().toString(),
                ((EditText) findViewById(R.id.editview_ingr)).getText().toString(),
                ((EditText) findViewById(R.id.editview_time)).getText().toString(),
                ((Spinner)  findViewById(R.id.spinner)).getSelectedItem().toString(),
                byteArray
     */
}